from django.conf.urls import url
from django.urls import path, re_path
from .views import TweetListView, TweetDetailView,TweetCreateView, TweetUpdateView, TweetDeleteView
# tweet_detail_view, tweet_list_view, tweet_create_view

urlpatterns = [
    
    re_path(r'^(?P<pk>\d+)/$', TweetDetailView.as_view(), name='detial-cbv'),
    re_path('(?P<pk>\d+)/update/', TweetUpdateView.as_view(), name='update'),
    re_path('list/',TweetListView.as_view(),name = 'list-cbv'),
    re_path('create/', TweetCreateView.as_view(), name = 'create'),
    re_path('(?P<pk>\d+)/delete/', TweetDeleteView.as_view(), name='delete'),

    # path('', tweet_detail_view, name='detail'),
    # path('', tweet_list_view, name='list'),
    # path('', tweet_create_view, name='create'),
]

app_name = "tweets"



